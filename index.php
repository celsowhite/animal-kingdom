<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
	<title>Animal Kingdom</title>          
	 <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width" />
	<meta name="author" content="Animal Kingdom">
	<meta name="keywords" content=""/>
	<meta name="description" content="ANIMAL KINGDOM was founded in September 2012 to develop, produce and finance feature films, television and digital content.">

	<!-- Included CSS Files -->
	<link rel="stylesheet" media="screen" href="css/animalkingdom.css" />
	<link rel="stylesheet" media="screen" href="css/foundation.css" />

	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
	<section id="main">
		<div class="row logo">
			<div class="twelve columns">
				<center><img src="images/animal-kingdom-logo.png"></center>
			</div>
		</div>

		<div class="row">
			<div class="seven columns extra">
				<p class="header1">About Us</p>
				<p>ANIMAL KINGDOM was founded in September 2012 to develop and produce feature films, television, and digital content. Based in New York City, the company is overseen by principals Joshua Astrachan, David Kaplan and Frederick W. Green.</p>
				<p class="header2">Our Films</p>
				<p class="role">The company's body of work includes writer/director Destin Daniel Cretton's SHORT TERM 12, which won both the Grand Jury Prize and the Audience Award at SXSW 2013; IT FOLLOWS which premiered as part of the Semaine de la Critique at the Cannes Film Festival before going on to become the highest grossing film in the history of RadiusTWC; Joachim Trier’s LOUDER THAN BOMBS which premiered in main competition at the 68th Cannes Film Festival in May 2015 and was released in April, 2016 by The Orchard; KICKS, which premiered at the Tribeca Film Festival where it won an award for cinematography and will be released by Focus World in 2016; and Jim Jarmusch’s PATERSON starring Adam Driver which premiered in main competition at the Cannes Film Festival in 2016 and will be released by Amazon Studios.</p> 
				<p class="role">Animal Kingdom is also in post-production on TRAMPS, Adam Leon's much anticipated sophomore follow up to his award winning first feature "Gimme the Loot” and TRESPASS AGAINST US, a crime thriller starring Michael Fassbender and Brendan Gleeson that features an original score by the Chemical Brothers and will be released by A24.</p>
				<p class="header2">Television</p>
				<p class="role">The company’s first television project is an adaptation of David Carr’s acclaimed memoir, THE NIGHT OF THE GUN - which is in development with Sony Pictures Television and AMC.  Bob Odenkirk is attached to play David Carr and Shawn Ryan and Eileen Meyers are attached as executive producers and writers.</p>
			</div>
			<div class="five columns">
				<p class="header2"><a href="http://shortterm12.com/" target="_blank">Short Term 12</a></p>

				<p class="quote">"The finest American drama so far this year."</p>
				<p class="citation">- David Edelstein, New York Magazine</p>

				<p class="quote">"A wonder. Exceptional, moving and intimate."</p>
				<p class="citation">- Kenneth Turan, The Los Angeles Times</p>

				<p class="header2"><a href="#">IT Follows</a></p>

				<p class="quote">"Remarkably ingenious and scalp-pricklingly scary."</p>
				<p class="citation">- Tim Robey, The Telegraph</p>

				<p class="quote">"One of the most striking American indie horror films in years"</p>
				<p class="citation">- The Dissolve</p>

				<p class="header2"><a href="#">Louder Than Bombs</a></p>

				<p class="quote">"A family drama of extraordinary beauty"​</p>
				<p class="citation">­- Joe Morganstern, The Wall Street Journal</p>

				<p class="quote">"Eloquent, startling, keenly observed."</p>
				<p class="citation">­- Sheri Linden, The Los Angeles Times</p>

				<p class="header2"><a href="#">KICKS</a></p>
 
				<p class="quote">"This is a debut of undeniable promise"</p>
				<p class="citation">­- Andrew Barker, Variety</p>
 
				<p class="quote">"Can movies like 'Kicks' usher in the next wave of Spike Lees?"</p> 
				<p class="citation">­- Steven Zeitchik, LA Times</p>

				<p class="header2"><a href="#">PATERSON</a></p>
 
				<p class="quote">“A delight: a prose-poem of gentle comic humility and acceptance of life.”</p> 
				<p class="citation">­- Peter Bradshaw, The Guardian</p>
				 
				<p class="quote">"Pure, filmic poetry… deceptively rich with its elements in perfect balance”</p>
				<p class="citation">­- Donald Clarke, The Irish Times</p>

			</div>
		</div>
		<div class="row">
			<div class="twelve columns footer">
				<p>info@animalkingdomfilms.com | 242 W 30th Street, Suite 602 | New York, New York 10001 | 212 206 1801 | <a href="https://twitter.com/AnimalFilms" target="_blank">@animalfilms</a></p>
			</div>
		</div>
	<section>

</body>
</html>    